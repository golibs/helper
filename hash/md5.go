package hash
import (
	"crypto/md5"
	"fmt"
	"encoding/hex"
	"github.com/vmihailenco/msgpack"
	"github.com/satori/go.uuid"
)

func MD5TObjects(texts ...interface{}) string {
	hasher := md5.New()
	bytes, err := msgpack.Marshal(texts)
	if err != nil {
		fmt.Errorf(" Can not Marshal in MD5Text")
		return uuid.NewV1().String()
	}
	hasher.Write(bytes)
	return hex.EncodeToString(hasher.Sum(nil))
}
