package jsonutils

import (
	"io/ioutil"
	"encoding/json"
	"github.com/tidwall/gjson"
	"bytes"
	"fmt"
)

func FileToObject(path string, v interface{}) error {
	file, e := ioutil.ReadFile(path)
	if e != nil {
		return e
	}
	return json.Unmarshal(file, &v)
}
func TextToObject(data []byte, v interface{}) error {
	return json.Unmarshal(data, &v)
}
func TextToResult(data []byte) gjson.Result {
	return gjson.ParseBytes(data)
}
func FileToResult(path string) (result gjson.Result, err error) {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return
	}
	result = gjson.ParseBytes(bytes)
	return
}
func ObjectToText(v interface{}) (string, error) {
	bytes, err := json.Marshal(&v)
	return string(bytes), err
}
func ObjectToTextIgnoreError(v interface{}) (string) {
	bytes, err := json.Marshal(&v)
	if err != nil {
		fmt.Errorf("Can not convert object to json. ")
		return ""
	}
	return string(bytes)
}
func JsonPrettyPrint(in string) string {
	var out bytes.Buffer
	err := json.Indent(&out, []byte(in), "", "\t")
	if err != nil {
		return in
	}
	return out.String()
}
func ObjectToPrettyText(v interface{}) (string, error) {
	text, err := ObjectToText(v)
	if err != nil {
		return text, err
	}
	return JsonPrettyPrint(text), err

}
func ObjectToPrettyTextIgnoreError(v interface{}) (string) {
	return JsonPrettyPrint(ObjectToTextIgnoreError(v))
}
