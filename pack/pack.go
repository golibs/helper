package pack

import (
	"github.com/vmihailenco/msgpack"
	"fmt"
	"github.com/satori/go.uuid"
)

func MarshalOrIgnore(v ...interface{}) string {
	bytes, err := msgpack.Marshal(v)
	if err != nil {
		fmt.Errorf(" Can not Marshal using random string")
		return uuid.NewV1().String()
	}
	return string(bytes)
}
func Marshal(v ...interface{}) (string, error) {
	bytes, err := msgpack.Marshal(v)
	return string(bytes), err
}
func UnMarshal(data []byte, v interface{}) error {
	return msgpack.Unmarshal(data, v)
}
func UnMarshalText(data string, v interface{}) error {
	return msgpack.Unmarshal([]byte(data), v)
}
