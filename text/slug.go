package text

import "github.com/gosimple/slug"

func Slug(text string) string {
	return slug.Make(text)
}
